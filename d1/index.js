// fetch() method in JavaScript is used to send request in the server and load the resceive response in the webpagas. The request and response is in JSON format.

// Syntax:
    // fetch("url", options)
        // url - this is the url which the request is to be made (endpoint).
        // options - array of properties that contains the HTTP method, body of request, headers.

// Get post data
fetch("https://jsonplaceholder.typicode.com/posts")
.then(res => res.json())
.then(data => showPosts(data));


// Add post data
document.querySelector("#form-add-post")
.addEventListener("submit", (e) => {
    // Prevents the page from loading
    e.preventDefault();
    fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
            title: document.querySelector("#txt-title").value,
            body: document.querySelector("#txt-body").value,
            userId: 1
        }),
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then((res) => res.json())
    .then((data) => {
        console.log(data);
        alert("Successfully added!");
    })

    // resets the state of our input into blanks after submitting a new post.
    document.querySelector("#txt-title").value = null;
    document.querySelector("#txt-body").value = null;

})

// View Posts
const showPosts = (posts) => {
    let postEntries = "";

    // We will used forEach() to display each movie inside our mock database.
    posts.forEach(post => {
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost('${post.id}')">Edit</button>
                <button onclick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });

    // To check what is stored in the postEntries variables
    // console.log(postEntries)

    // To replace the content of the "div-post-entries"
    document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// Edit Post Button
const editPost = (id) => {
    // Contain the value of the title and body in a variable.
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    // Pass the id, title, and body of the moview post to be updated in the Edit Post/Form.
    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;

    // To removed the disable property
    document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

// Update post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();
    let id = document.querySelector("#txt-edit-id").value;
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
        method: "PUT",
        body: JSON.stringify({
            id: id,
            title: document.querySelector("#txt-edit-title"),
            body: document.querySelector("#txt-edit-body").value,
            userId: 1
        }),
        headers:{
            "Content-Type": "application/json"
        }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        alert("Successfully updated")

        document.querySelector("#txt-edit-title").value = null;
        document.querySelector("#txt-edit-body").value = null;

        // Add an attribute in a HTML element 
        document.querySelector("#btn-submit-update").setAttribute("disabled", true)
    })
   
});

// Delete post
/*
s43 Activity:
    -Using the code so far, write the necessary code to delete a post.
    -Create the function (deletePost(id)) invoked on the “onclick” event of the delete button.
    -Make sure that the Now Showing List will be updated upon clicking the delete button.
    -Once done create a remote link and push to git with the commit message of WDC028-43.
    -Add the link in Boodle.
*/
const deletePost = (id) => {
    let postId = posts.indexOf(id);
    posts.splice(postId, 1);
    showPosts();
}

/*------ Solution By Sir Angelito Quiambao -----*/
// const deletePost = (id) => {
//     // filter method will save the posts that are not equal to the id parameter.
//     posts = posts.filter((post) => {
//         if(post.id != id){
//             return post;
//         } 
//     })

//     console.log(posts);
//     // .remove() method removes an elemet (or node) from the document.
//     document.querySelector(`#post-${id}`).remove();
// }